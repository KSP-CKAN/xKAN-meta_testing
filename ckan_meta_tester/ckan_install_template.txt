install -c $ckanfile --headless
list --porcelain
show --with-versions $identifier
remove $identifier --headless
